import { createRouter,createWebHistory, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
import ArtHouse from './views/Impact/ArtHouse.vue';
import VirginiaPark from './views/VirginiaCommunity.vue';
import Euclid1151 from './views/Impact/Euclid1151.vue';
import Park360 from './views/Impact/360Park.vue';
import Euclid1167 from './views/Impact/Euclid1167.vue';
import Holland from './views/Impact/HollandMaze.vue';
import Euclid1189 from './views/Impact/Euclid1189.vue';
import { globalStore } from './store';
const mainTitle = '360 Detroit, Inc.'


export const routes : IRoute =  
  {
    home:  {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        title: `Home - ${mainTitle}`
      }
    },
    about:  {
      path: '/about',
      name: 'about',
      component: About,
      meta: {
        title: `About Us - ${mainTitle}`
      }
    },
    virginiaPark: {
      path: '/virginia-park-community',
      name: 'virginiaPark',
      component: VirginiaPark,
      meta: {
        title: `Virginia Park Community - ${mainTitle}`
      }
    },
    arthouse: {
      path: '/impact/art-house',
      name: 'arthouse',
      component: ArtHouse,
      meta: {
        title: `Art House - ${mainTitle}`
      }
    },
    euclid1151: {
      path: '/impact/1151-euclid',
      name: 'euclid1151',
      component: Euclid1151,
      meta: {
        title: `Euclid 1151 - ${mainTitle}`
      }
    },
    euclid1167: {
      path: '/impact/1167-euclid',
      name: 'euclid1167',
      component: Euclid1167,
      meta: {
        title: `Euclid 1167 - ${mainTitle}`
      }
    },
    euclid1189: {
      path: '/impact/1189-euclid',
      name: 'euclid1189',
      component: Euclid1189,
      meta: {
        title: `Euclid 1189 - ${mainTitle}`
      }
    },
    hollandMaze: {
      path: '/impact/holland',
      name: 'holland',
      component: Holland,
      meta: {
        title: `Holland Maze - ${mainTitle}`
      }
    },
    park360: {
      path: '/impact/360-park',
      name: '360park',
      component: Park360,
      meta: {
        title: `360 Park - ${mainTitle}`
      }
    },
  };


const routeHistory = import.meta.env.VITE_CORDOVA ? createWebHashHistory() : createWebHistory();
// const routeHistory = createWebHashHistory();
const router = createRouter({
  history: routeHistory,
  routes: Object.values(routes),
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
})

router.beforeEach((to,from,next) => {
  document.title = to.meta.title;
  globalStore.state.menuIsOpen = false;
  next();
})
router.afterEach((to,from,next) => {
  window.scrollTo({top:0});
  next();
})

export default router;

export interface IRoute {
  home: RouteRecordRaw,
  euclid1151: RouteRecordRaw,
  euclid1167: RouteRecordRaw,
  euclid1189: RouteRecordRaw,
  virginiaPark: RouteRecordRaw,
  park360: RouteRecordRaw,
  hollandMaze: RouteRecordRaw,
  arthouse: RouteRecordRaw,
  about: RouteRecordRaw
}